<?php

namespace Drupal\nodeorder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nodeorder\ConfigManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides forms for managing Node Order.
 */
class NodeorderAdminForm extends ConfigFormBase {

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The nodeorder config manager.
   *
   * @var \Drupal\nodeorder\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->configManager = $container->get('nodeorder.config_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodeorder_admin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configManager->config();

    $form['nodeorder_show_links'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display ordering links'),
      '#description' => $this->t('Choose whether to show ordering links. Links can be shown for all categories associated to a node or for the currently active category. It is also possible to not show the ordering links at all.'),
    ];

    $form['nodeorder_show_links']['nodeorder_show_links_on_node'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose how to display ordering links'),
      '#default_value' => $config->get(ConfigManagerInterface::KEY_SHOW_LINKS_ON_NODE),
      '#description' => $this->t('When displaying links based on the context, they will only be shown on taxonomy and nodeorder pages.'),
      '#options' => [
        $this->t("Don't display ordering links"),
        $this->t('Display ordering links for all categories'),
        $this->t('Display ordering links based on the active category'),
      ],
    ];

    $bundles = $this->entityTypeBundleInfo->getBundleInfo('taxonomy_term');
    $form['nodeorder_vocabularies'] = [
      '#title' => $this->t('Vocabularies'),
      '#description' => $this->t('Select vocabularies for which the order can be changed.'),
      '#default_value' => $config->get(ConfigManagerInterface::KEY_VOCABULARIES),
      '#type' => 'checkboxes',
      '#options' => array_combine(array_keys($bundles), array_column($bundles, 'label')),
    ];

    $form['nodeorder_link_to_ordering_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display link to the ordering page'),
      '#description' => $this->t('If enabled, a tab will appear on all <em>nodeorder/term/%</em> and <em>taxonomy/term/%</em> pages that quickly allows administrators to get to the node ordering administration page for the term.'),
      '#default_value' => $config->get(ConfigManagerInterface::KEY_LINK_TO_ORDERING_PAGE),
    ];

    $form['nodeorder_link_to_ordering_page_taxonomy_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display link to the ordering page on taxonomy administration page'),
      '#description' => $this->t('If enabled, a tab will appear on <em>admin/content/taxonomy/%</em> pages that quickly allows administrators to get to the node ordering administration page for the term.'),
      '#default_value' => $config->get(ConfigManagerInterface::KEY_LINK_TO_ORDERING_PAGE_TAXONOMY_ADMIN),
    ];

    $form['nodeorder_override_taxonomy_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override the default taxonomy page with one from nodeorder'),
      '#description' => $this->t('Disabling this will allow the panels module to override taxonomy pages instead. See <a href="https://www.drupal.org/node/1713048">this issue</a> for more information. You will have to clear caches for the change to take effect.'),
      '#default_value' => $config->get(ConfigManagerInterface::KEY_OVERRIDE_TAXONOMY_PAGE),
    ];

    $form['entity_list_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Max entities per page'),
      '#description' => $this->t('How many entities can be listed on a page.'),
      '#default_value' => $config->get(ConfigManagerInterface::KEY_ENTITY_LIST_LIMIT),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // ...
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Trigger weight update for all nodes in a vocabulary's term tree.
    // @see nodeorder_taxonomy_vocabulary_form_submit.
    // @see https://www.drupal.org/project/nodeorder/issues/3415452
    $vocabularies = array_filter($form_state->getValue('nodeorder_vocabularies'));

    $this->configManager->updateConfigValues([
      ConfigManagerInterface::KEY_SHOW_LINKS_ON_NODE => $form_state->getValue('nodeorder_show_links_on_node'),
      ConfigManagerInterface::KEY_LINK_TO_ORDERING_PAGE => $form_state->getValue('nodeorder_link_to_ordering_page'),
      ConfigManagerInterface::KEY_LINK_TO_ORDERING_PAGE_TAXONOMY_ADMIN => $form_state->getValue('nodeorder_link_to_ordering_page_taxonomy_admin'),
      ConfigManagerInterface::KEY_OVERRIDE_TAXONOMY_PAGE => $form_state->getValue('nodeorder_override_taxonomy_page'),
      ConfigManagerInterface::KEY_ENTITY_LIST_LIMIT => $form_state->getValue('entity_list_limit'),
      ConfigManagerInterface::KEY_VOCABULARIES => $vocabularies,
    ]);

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [ConfigManagerInterface::CONFIG_NAME];
  }

}
