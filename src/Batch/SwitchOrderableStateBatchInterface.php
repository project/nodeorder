<?php

namespace Drupal\nodeorder\Batch;

/**
 * Interface for switch orderable state batch implementations.
 */
interface SwitchOrderableStateBatchInterface {

  /**
   * Process term batch callback.
   *
   * @param int|string $vocabularyId
   *   An id of vocabulary.
   * @param int|string $termId
   *   An id of term.
   * @param array $context
   *   Batch context.
   *
   * @return void
   */
  public static function processTerm($vocabularyId, $termId, array &$context);

  /**
   * Update config batch callback.
   *
   * @param int|string $vocabularyId
   *   An id of vocabulary.
   *
   * @return void
   */
  public static function updateConfig($vocabularyId);

  /**
   * Finis batch callback.
   *
   * @param bool $success
   *   Indicate that the batch API tasks were all completed successfully.
   * @param array $results
   *   An array of all the results that were updated in update_do_one().
   * @param array $operations
   *   A list of the operations that had not been completed by the batch API.
   *
   * @return void
   */
  public static function finish($success, $results, $operations);

}
