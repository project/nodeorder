<?php

namespace Drupal\nodeorder\Batch;

use Drupal\Core\Cache\Cache;

/**
 * Switching vocabulary from non-orderable to orderable.
 */
class SwitchToOrderableBatch implements SwitchOrderableStateBatchInterface {

  /**
   * {@inheritdoc}
   */
  public static function processTerm($vocabularyId, $termId, array &$context) {
    $nodes = static::getTermNodes($termId);

    foreach ($nodes as $node) {
      static::processTermNode($termId, $node->nid);
    }
  }

  /**
   * Retrieve *current* nodes for the current term.
   *
   * @todo Replace this hacked function call.
   *
   * @param int|string $termId
   *   An id of term.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   A resource identifier pointing to the query results.
   */
  public static function getTermNodes($termId) {
    $order = 'n.sticky DESC, tn0.weight';

    /** @var \Drupal\nodeorder\NodeOrderManagerInterface $manager */
    $manager = \Drupal::service('nodeorder.manager');
    return $manager->selectNodes([$termId], 'and', 0, FALSE, $order, 0);
  }

  /**
   * Update node weight in term index.
   *
   * @param int|string $termId
   *   An id of term.
   * @param int|string $nodeId
   *   An id of node.
   *
   * @return void
   */
  public static function processTermNode($termId, $nodeId) {
    $maxQuery = \Drupal::database()
      ->select('taxonomy_index', 'ti')
      ->condition('tid', $termId);
    $maxQuery->addExpression('MAX(weight)', 'mweight');
    $currentMaxWeight = $maxQuery->execute()->fetchField();

    $newMaxWeight = (int) $currentMaxWeight + 1;

    \Drupal::database()->update('taxonomy_index')
      ->condition('nid', $nodeId)
      ->condition('tid', $termId)
      ->fields(['weight' => $newMaxWeight])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public static function updateConfig($vocabularyId) {
    /** @var \Drupal\nodeorder\ConfigManagerInterface $configManager */
    $configManager = \Drupal::service('nodeorder.config_manager');
    $configManager->updateOrderableValue($vocabularyId, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public static function finish($success, $results, $operations) {
    Cache::invalidateTags(['nodeorder']);

    \Drupal::messenger()
      ->addStatus(t('You may now order nodes within this vocabulary.'));
  }

}
