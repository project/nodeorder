<?php

namespace Drupal\nodeorder\Batch;

use Drupal\Core\Cache\Cache;

/**
 * Switching vocabulary from orderable to non-orderable.
 */
class SwitchToNonOrderableBatch implements SwitchOrderableStateBatchInterface {

  /**
   * {@inheritdoc}
   */
  public static function processTerm($vocabularyId, $termId, array &$context) {}

  /**
   * {@inheritdoc}
   */
  public static function updateConfig($vocabularyId) {
    /** @var \Drupal\nodeorder\ConfigManagerInterface $configManager */
    $configManager = \Drupal::service('nodeorder.config_manager');
    $configManager->updateOrderableValue($vocabularyId, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public static function finish($success, $results, $operations) {
    Cache::invalidateTags(['nodeorder']);

    \Drupal::messenger()
      ->addStatus(t('You may no longer order nodes within this vocabulary.'));
  }

}
