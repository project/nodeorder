<?php

namespace Drupal\nodeorder;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * TermTreeLoader service to handle tree load.
 */
class TermTreeLoader implements TermTreeLoaderInterface {

  /**
   * Taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * TermTreeLoader construct.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public function descendantTids($vid, $parent = 0, $maxDepth = NULL) {
    $tree = $this->termStorage->loadTree($vid, $parent, $maxDepth, FALSE);

    return array_map(function ($value) {
      return $value->tid;
    }, $tree);
  }

  /**
   * {@inheritdoc}
   */
  public function descendantTidsByTermId($termId, $maxDepth = NULL) {
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = $this->termStorage->load($termId);

    return $this->descendantTids($term->bundle(), $termId, $maxDepth);
  }

}
