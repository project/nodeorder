<?php

namespace Drupal\nodeorder;

/**
 * Provides an interface defining a TermTreeLoader.
 */
interface TermTreeLoaderInterface {

  /**
   * Finds all descendant term ids in a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   * @param int $parent
   *   The term ID under which to generate the tree. If 0, generate the tree
   *   for the entire vocabulary.
   * @param int $maxDepth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   *
   * @return object[]|\Drupal\taxonomy\TermInterface[]
   *   A numerically indexed array of term objects that are the children of the
   *   vocabulary $vid.
   */
  public function descendantTids($vid, $parent = 0, $maxDepth = NULL);

  /**
   * Finds all descendant term ids.
   *
   * @param string|int $termId
   *   The term ID under which to generate the tree.
   * @param int $maxDepth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   *
   * @return object[]|\Drupal\taxonomy\TermInterface[]
   *   A numerically indexed array of term objects that are the children of the
   *   vocabulary $vid.
   */
  public function descendantTidsByTermId($termId, $maxDepth = NULL);

}
