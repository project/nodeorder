<?php

namespace Drupal\nodeorder;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines a service that manage nodeorder configs.
 */
class ConfigManager implements ConfigManagerInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ConfigManager object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function config() {
    // During a single PHP process the values can be changed many times,
    // so it is important to get the actual values.
    return $this->configFactory->getEditable(ConfigManagerInterface::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrderableValue(string $vocabularyId, bool $isOrderable) {
    $config = $this->config();

    $vocabularies = $config->get(ConfigManagerInterface::KEY_VOCABULARIES);
    if ($isOrderable) {
      $vocabularies[$vocabularyId] = $vocabularyId;
    }
    else {
      unset($vocabularies[$vocabularyId]);
    }

    $config->set(ConfigManagerInterface::KEY_VOCABULARIES, $vocabularies);
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function updateConfigValues(array $data) {
    $config = $this->config();

    foreach (ConfigManagerInterface::CONFIG_KEYS as $key) {
      if (isset($data[$key])) {
        $config->set($key, $data[$key]);
      }
    }

    $config->save();
  }

}
