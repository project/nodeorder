<?php

namespace Drupal\nodeorder;

/**
 * Provides an interface defining a ConfigManager.
 */
interface ConfigManagerInterface {
  public const CONFIG_NAME = 'nodeorder.settings';
  public const KEY_SHOW_LINKS_ON_NODE = 'show_links_on_node';
  public const KEY_LINK_TO_ORDERING_PAGE = 'link_to_ordering_page';
  public const KEY_LINK_TO_ORDERING_PAGE_TAXONOMY_ADMIN = 'link_to_ordering_page_taxonomy_admin';
  public const KEY_OVERRIDE_TAXONOMY_PAGE = 'override_taxonomy_page';
  public const KEY_ENTITY_LIST_LIMIT = 'entity_list_limit';
  public const KEY_VOCABULARIES = 'vocabularies';
  public const CONFIG_KEYS = [
    self::KEY_SHOW_LINKS_ON_NODE,
    self::KEY_LINK_TO_ORDERING_PAGE,
    self::KEY_LINK_TO_ORDERING_PAGE_TAXONOMY_ADMIN,
    self::KEY_OVERRIDE_TAXONOMY_PAGE,
    self::KEY_ENTITY_LIST_LIMIT,
    self::KEY_VOCABULARIES,
  ];

  /**
   * Retrieve 'nodeorder.settings' configuration object.
   *
   * @return \Drupal\Core\Config\Config
   *   A configuration object.
   */
  public function config();

  /**
   * Updates the orderable status of a vocabulary.
   */
  public function updateOrderableValue(string $vocabularyId, bool $isOrderable);

  /**
   * Updates configuration values.
   */
  public function updateConfigValues(array $data);

}
