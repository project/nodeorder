# NODE ORDER MODULE FOR DRUPAL

## Introduction

The nodeorder module gives users an easy way to order nodes within their
taxonomy terms.

By default, the taxonomy module orders listings of nodes by stickiness and
then by node creation date -- most recently posted nodes come first.

The nodeorder module gives the user the ability to put nodes in any order they
wish within each category that the node lives.

## Installation and Configuration

### Installation Instructions
1. If your site is managed via Composer, use Composer to download the module.
Otherwise, copy/upload the module to the modules directory of your
Drupal installation.
2. Enable the module.
3. Configure user permissions.
4. Turn on "orderability" on a per-vocabulary basis by visiting your
vocabularies' administration pages (admin/structure/taxonomy).
This module adds a checkbox on the "edit vocabulary" page titled "Orderable".
After checking this box and saving your changes, you'll be able to order
nodes that are classified in this category.

### Usage Instructions
- To use drag and drop node ordering, you will find a tab called "order" on any
taxonomy/term/[tid]/edit pages where the vocabulary has been set to orderable.

### Configuration
- Navigate to admin/config/content/nodeorder, where you can set some options
that determine the way nodeorder works.

### Views Integration
- Edit the 'Taxonomy term' (views.view.taxonomy_term) view, and any other
  applicable taxonomy views and add the 'Nodeorder' sort item.

## TECHNICAL NOTES

Upon installation, this module adds a new column (weight) to the
taxonomy_index table.  Adding a column to a core table?  Are you crazy?  Yeah,
I guess so ... but it lets us keep the module's code very small since
most everything works through taxonomy.  Also it helps to avoid an extra
join for every node listing.

Please note that the node order is only respected when visiting links
that begin with "nodeorder" -- if you visit links that begin with
"taxonomy" they will appear in the generic taxonomy order.  Since the
module implements hook_term_path, the taxonomy links that get printed
per node will correctly point to the "nodeorder" space when they are in
orderable vocabularies.

The nodeorder module was developed by FunnyMonkey.

## ToDos

* Add links to node view for ordering up and down
