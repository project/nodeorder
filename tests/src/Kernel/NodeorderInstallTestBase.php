<?php

namespace Drupal\Tests\nodeorder\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Base class for module installation and uninstallation tests.
 */
abstract class NodeorderInstallTestBase extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'filter', 'text', 'taxonomy', 'user'];

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstaller
   */
  protected $moduleInstaller;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->moduleInstaller = $this->container->get('module_installer');
    $this->database = $this->container->get('database');

    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);

    $this->installEntitySchema('taxonomy_term');

    $this->installConfig(['filter', 'user', 'taxonomy']);

    $this->moduleInstaller->install(['nodeorder']);
  }

}
