<?php

namespace Drupal\Tests\nodeorder\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests TermTreeLoader service.
 *
 * @group nodeorder
 */
class TermTreeLoaderTest extends KernelTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['taxonomy', 'text', 'filter', 'user', 'node', 'nodeorder'];

  /**
   * Term tree loader.
   *
   * @var \Drupal\nodeorder\TermTreeLoaderInterface
   */
  protected $treeLoader;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('taxonomy_term');
    $this->installConfig(['filter']);

    $this->treeLoader = $this->container->get('nodeorder.term_tree_loader');
  }

  /**
   * Tests both methods descendantTids and descendantTidsByTermId.
   */
  public function testDescendantTidsSelection() {
    $vocabulary1 = $this->createVocabulary(['vid' => 'vid1', 'name' => 'name1']);
    $vocabulary2 = $this->createVocabulary(['vid' => 'vid2', 'name' => 'name2']);

    $term1v1 = $this->createTerm($vocabulary1, ['name' => 'v1t1']);
    $term11v1 = $this->createTerm($vocabulary1, ['name' => 'v1t11', 'parent' => $term1v1->id()]);
    $term111v1 = $this->createTerm($vocabulary1, ['name' => 'v1t111', 'parent' => $term11v1->id()]);
    $term2v1 = $this->createTerm($vocabulary1, ['name' => 'v1t2']);
    $term21v1 = $this->createTerm($vocabulary1, ['name' => 'v1t21', 'parent' => $term2v1->id()]);

    $this->createTerm($vocabulary2, ['name' => 'v2t1']);

    // Case1: vid - vid1, parent - 0, maxDepth -  null.
    $this->assertEquals(
      [
        $term1v1->id(),
        $term11v1->id(),
        $term111v1->id(),
        $term2v1->id(),
        $term21v1->id(),
      ],
      $this->treeLoader->descendantTids($vocabulary1->id())
    );

    // Case2: vid - vid1, parent - 0, maxDepth - 1.
    $this->assertEquals(
      [
        $term1v1->id(),
        $term2v1->id(),
      ],
      $this->treeLoader->descendantTids($vocabulary1->id(), 0, 1)
    );

    // Case3: vid - vid1, parent - v1t2, maxDepth - null.
    $this->assertEquals(
      [
        $term21v1->id(),
      ],
      $this->treeLoader->descendantTids($vocabulary1->id(), $term2v1->id())
    );

    $this->assertEquals(
      [
        $term11v1->id(),
        $term111v1->id(),
      ],
      $this->treeLoader->descendantTidsByTermId($term1v1->id())
    );

    $this->assertEquals(
      [
        $term11v1->id(),
      ],
      $this->treeLoader->descendantTidsByTermId($term1v1->id(), 1)
    );
  }

}
