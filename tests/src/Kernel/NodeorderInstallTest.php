<?php

namespace Drupal\Tests\nodeorder\Kernel;

/**
 * Tests module installation.
 *
 * @group nodeorder
 */
class NodeorderInstallTest extends NodeorderInstallTestBase {

  /**
   * Tests module installation.
   */
  public function testInstall() {
    $schema = $this->database->schema();

    $column_exists = $schema->fieldExists('taxonomy_index', 'weight');
    static::assertTrue($column_exists);

    $index_exists = $schema->indexExists('taxonomy_index', 'weight');
    static::assertTrue($index_exists);

    $extension_config = $this->container->get('config.factory')->getEditable('core.extension');
    static::assertEquals($extension_config->get('module.nodeorder'), 5);
  }

}
