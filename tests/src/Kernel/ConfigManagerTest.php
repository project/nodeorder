<?php

namespace Drupal\Tests\nodeorder\Kernel;

use Drupal\Core\Config\Config;
use Drupal\KernelTests\KernelTestBase;
use Drupal\nodeorder\ConfigManagerInterface;

/**
 * @covers \Drupal\nodeorder\ConfigManager
 * @group nodeorder
 */
class ConfigManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['nodeorder'];

  /**
   * The nodeorder configuration manager.
   *
   * @var \Drupal\nodeorder\ConfigManager
   */
  protected $configManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->configManager = $this->container->get('nodeorder.config_manager');
  }

  /**
   * {@inheritdoc}
   */
  public function testConfig() {
    $this->configManager->updateOrderableValue('test', TRUE);

    $config = $this->configManager->config();
    static::assertInstanceOf(Config::class, $config);

    $actualValues = $config->get(ConfigManagerInterface::KEY_VOCABULARIES);
    static::assertEquals(['test' => 'test'], $actualValues);
  }

  /**
   * {@inheritdoc}
   */
  public function testUpdateOrderableValue() {
    $actualValues = function () {
      return $this->configManager->config()->get(ConfigManagerInterface::KEY_VOCABULARIES);
    };

    $this->configManager->updateOrderableValue('test2', TRUE);
    $this->configManager->updateOrderableValue('test3', TRUE);
    static::assertEquals(['test2' => 'test2', 'test3' => 'test3'], $actualValues());

    $this->configManager->updateOrderableValue('test3', FALSE);
    static::assertEquals(['test2' => 'test2'], $actualValues());

    $this->configManager->updateOrderableValue('test2', FALSE);
    static::assertEquals([], $actualValues());
  }

  /**
   * {@inheritdoc}
   */
  public function testUpdateConfigValues() {
    $this->configManager->updateConfigValues([
      'testK' => 'testV',
      ConfigManagerInterface::KEY_VOCABULARIES => ['test4' => 'test4'],
    ]);

    $actualConfigData = $this->configManager->config()->get();

    static::assertArrayNotHasKey('testK', $actualConfigData);
    static::assertArrayHasKey(ConfigManagerInterface::KEY_VOCABULARIES, $actualConfigData);
    static::assertEquals(['test4' => 'test4'], $actualConfigData[ConfigManagerInterface::KEY_VOCABULARIES]);
  }

}
