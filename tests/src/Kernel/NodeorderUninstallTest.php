<?php

namespace Drupal\Tests\nodeorder\Kernel;

/**
 * Tests the uninstallation of module.
 *
 * @group nodeorder
 */
class NodeorderUninstallTest extends NodeorderInstallTestBase {

  /**
   * Tests module uninstallation.
   */
  public function testUninstall() {
    $schema = $this->database->schema();

    // Need to ensure that filed and index exists before uninstallation.
    // Otherwise test will be successfully passed every time.
    // @see: \Drupal\Tests\nodeorder\Kernel\NodeorderInstallTest::testInstall().
    static::assertTrue($schema->fieldExists('taxonomy_index', 'weight'));
    static::assertTrue($schema->indexExists('taxonomy_index', 'weight'));

    $this->moduleInstaller->uninstall(['nodeorder']);

    static::assertFalse($schema->fieldExists('taxonomy_index', 'weight'));
    static::assertFalse($schema->indexExists('taxonomy_index', 'weight'));
  }

}
