<?php

namespace Drupal\Tests\nodeorder\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\nodeorder\ConfigManagerInterface;
use Drupal\nodeorder\NodeOrderListBuilder;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Tests NodeOrderListBuilder service.
 *
 * @group nodeorder
 */
class NodeOrderListBuilderTest extends KernelTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'text',
    'filter',
    'user',
    'node',
    'taxonomy',
    'nodeorder',
  ];

  /**
   * The nodeorder config manager.
   *
   * @var \Drupal\nodeorder\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installSchema('user', ['users_data']);
    $this->installConfig(['system', 'filter', 'user', 'taxonomy']);

    $this->container->get('module_installer')->install(['nodeorder']);

    $this->configManager = $this->container->get('nodeorder.config_manager');
    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
  }

  /**
   * Tests limit config for list builder.
   *
   * @dataProvider limitConfigDataProvider
   */
  public function testLimitConfig(int $config_value, int $expected_limit) {
    $entity_type = $this->nodeStorage->getEntityType();
    $taxonomy_term = $this->createTerm($this->createVocabulary());

    $this->configManager->updateConfigValues([
      ConfigManagerInterface::KEY_ENTITY_LIST_LIMIT => $config_value,
    ]);

    $builder = NodeOrderListBuilder::createInstance($this->container, $entity_type, $taxonomy_term);
    self::assertEquals($expected_limit, $this->getProperty($builder, 'limit'));
  }

  /**
   * Data provider for limitConfig with possible cases.
   *
   * @return array
   *   A list of test case arguments.
   */
  public function limitConfigDataProvider() {
    // $config_value, $expected_limit.
    return [
      [0, 50],
      [50, 50],
      [100, 100],
    ];
  }

  /**
   * Gets the value of protected property.
   *
   * @param $object
   *   Object.
   * @param $property
   *   Property name.
   *
   * @return mixed
   *   Property value.
   *
   * @throws \ReflectionException
   */
  protected function getProperty($object, $property) {
    $reflectedClass = new \ReflectionClass($object);
    $reflection = $reflectedClass->getProperty($property);
    $reflection->setAccessible(TRUE);
    return $reflection->getValue($object);
  }

}
